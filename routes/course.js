const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");


// Route for creating a course
router.post("/",  (req, res) => {

	
	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));

});

// Route for retrieving all the courses
router.get("/all", (req, res) => { //need for middleware

	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));

});

// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;
