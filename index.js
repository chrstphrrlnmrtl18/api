const express = require("express");
// Creates an "app" variable that stores the result of the "express" function that initializes our express application and allows us access to different methods that will make backend creation easy
const app = express();
const cors = require("cors");
const mongoose = require("mongoose");
const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");

// Connect to our MongoDB database
mongoose.connect("mongodb+srv://admin:admin@batch243.ijt8xlq.mongodb.net/CourseBookingAPI?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
		});
	// Prompts a message in the terminal once the connection is "open" and we are able to successfully connect to our database
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoutes);


// Defines the "/courses" string to be included for all course routes defined in the "course" route file
app.use("/courses", courseRoutes);


// Will used the defined port number for the application whenever an environment variable is available OR will used port 4000 if none is defined
// This syntax will allow flexibility when using the application locally or as a hosted application

app.listen(process.env.PORT || 4000, () => {
		    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
		});